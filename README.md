# Accessories Vote Bar Charts in Excel

`data/2023-02-13_data_vote_close.json` is the data for votes during the initial month.
(cutoff timestamp is 1676294580)

This is the query used to pull data: [Snapshot GraphQL Query](https://hub.snapshot.org/graphql?query=query%20Votes%20%7B%0A%20%20votes%20(%0A%20%20%20%20first%3A%201000%0A%20%20%20%20skip%3A%200%0A%20%20%20%20where%3A%20%7B%0A%20%20%20%20%20%20proposal%3A%20%220xd0e15246eeb5743f2bc677a951b71e7148ba50c117eb610e6c025decc0acd2bd%22%0A%20%20%20%20%7D%0A%20%20%20%20orderBy%3A%20%22created%22%2C%0A%20%20%20%20orderDirection%3A%20desc%0A%20%20)%20%7B%0A%20%20%20%20id%0A%20%20%20%20voter%0A%20%20%20%20vp%0A%20%20%20%20created%0A%20%20%20%20proposal%20%7B%0A%20%20%20%20%20%20id%0A%20%20%20%20%7D%0A%20%20%20%20choice%0A%20%20%20%20space%20%7B%0A%20%20%20%20%20%20id%0A%20%20%20%20%7D%0A%20%20%7D%0A%7D)

You may have to reconnect the data file `data/2023-02-13_data_vote_close.json` in the Excel file if using on your local system.
Title font used in the Excel file is [IM Fell English](https://fonts.google.com/specimen/IM+Fell+English)